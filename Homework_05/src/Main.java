public class Main {
    public static void main(String[] args) {
        ATM marsel = new ATM();
        marsel.height = 1.85;
        marsel.isWorker = true;

        ATM notMarsel = new ATM();
        notMarsel.height = 1.7;
        notMarsel.isWorker = false;

        marsel.grow(0.05);
        marsel.relax();

        notMarsel.grow(0.3);
        notMarsel.work();

        System.out.println(marsel.height + " " + marsel.isWorker);
        System.out.println(notMarsel.height + " " + notMarsel.isWorker);
    }
}
