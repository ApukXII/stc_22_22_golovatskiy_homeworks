public class Main {
    public static void main(String[] args) {
        findLocalMin(new int[] {12, 6, 8, 11, 22, 5, 8, 15, 22, 0});
    }
    static void findLocalMin(int[] arr) {
        int i = 0, count = 0, left = 0, mid = 0, right = 0;

        while (i != arr.length) {
            left = mid;
            mid = right;
            right = arr[i++];
            if (right > mid && mid < left) {
                System.out.println("local min: " + mid);
                count++;
            }
        }
        if (i > 1 && right < mid) {
            System.out.println("local min: " + right);
            count++;
        }
        System.out.println("Count of local min: " + count);
    }
}