public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        if (from > to) {
            return -1;
        }
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }

        return sum;

    }

    public static void outputIsEvenNumbersArray(int[] a, int from, int to) {
        for (int i = from; i <= to; i++) {

            if (a[i] % 2 == 0) {
                System.out.println(a[i]);
            }

        }
    }

    public static void main(String[] args) {
        int[] array = {4, 53, 6, -8, -27, 18, 90, 102, 54, 40, 73};

        int sum = calcSumOfArrayRange(array, 4,9);

        System.out.println(sum);

        outputIsEvenNumbersArray(array, 0, array.length -1);

    }
}
